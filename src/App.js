import React ,{useMemo,useState,useEffect} from 'react'
import './App.css';
import "bootstrap/dist/css/bootstrap.css"
import Login from './Components/Login'
import Content from './Pages/Content';
import PolPage2 from './Pages/PolPage2'

import {BrowserRouter as Router,Route,Routes} from 'react-router-dom'
import ShowPage from './Pages/ShowPage';
import Nave from './Components/Nav';
import Footer from './Components/Footer';
import axios from "axios";
import RepTable from "./Report/RepTable";

import EditPage from './Pages/EditPage';
import Editpage2 from './Pages/Editpage2';







function App() {

  return (
   
   <Router>
    <div className="app">
       <Nave/>
       <Content />
       <Footer />
      </div>
   </Router>
     
   
  );
}

export default App;
