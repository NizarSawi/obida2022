import React,{useState} from 'react'
import * as Faicons from 'react-icons/all'
import {Link,NavLink} from 'react-router-dom'
import {SidebarData} from './SidebarData'
import '../Style/Sidebar.css'
import navlogo from '../Images/navlogo.png'
import { NavDropdown, MenuItem } from "react-bootstrap";


function Sidebar() {
    const [navbar,setNavbar]=useState(true)

    return (
        <>
            
         <nav className={navbar ?'nav-menu active' : 'nave-menu'}>
         <h3 style={{color:"white",fontFamily:"cursive"}} className=" shadow-0  text-center   "> بوابة المسافر للتأمين
            <hr/>
           
               </h3>
             <ul className="nav-menu-items">
            
               
             

                    {SidebarData.map((item,index)=>{
                        return(
                            <li key={index} className={item.cName}>
                              <Link to={item.path}>
                                 <span className={'icon'}>{item.icon}</span> 
                                  <span>{item.name}</span>
                              </Link>
                            </li>
                        )
                    })}
                 
                   
             </ul>
         </nav>
        </>
    )
}

export default Sidebar
