import React from 'react'
import { Bar } from 'react-chartjs-2'
import Chart from 'chart.js/auto'
import {CategoryScale} from 'chart.js'; Chart.register(CategoryScale)


const BarChart = () => {
  return (
    <div>
      <Bar
        data={{
          labels: ['شهر 12','شهر 11','شهر 10','شهر 9','شهر 8','شهر7','شهر 6', 'شهر 5', 'شهر4', 'شهر 3', 'شهر 2', 'شهر 1'],
          datasets: [
            {
              label: 'تأمينات سيارات',
              data: [12, 19, 3, 5, 2, 3,44,55,66,77,8,9,66,60],
              backgroundColor: [
               
                'rgba(54, 162, 235, 0.2)',
               
              ],
              borderColor: [
                'rgba(255, 99, 132, 1)',
               
              ],
              borderWidth: 1,
            },
             {
               label: 'تأمينات عامة',
               data: [47, 52, 67, 58, 9, 50],
               backgroundColor: 'orange',
               borderColor: 'red',
             },
          ],
        }}
        height={400}
        width={600}
        options={{
          maintainAspectRatio: false,
          scales: {
            yAxes: [
              {
                ticks: {
                  beginAtZero: true,
                },
              },
            ],
          },
          legend: {
            labels: {
              fontSize: 25,
            },
          },
        }}
      />
    </div>
  )
}

export default BarChart