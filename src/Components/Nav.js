import React from 'react'
import {Image, Navbar, Nav ,NavDropdown } from 'react-bootstrap'
import * as Faicons from 'react-icons/all'
import user from '../Images/user.jpg'
import navlogo from '../Images/navlogo.png'
import '../Style/Nav.css'
import { Link ,useNavigate} from 'react-router-dom'
import { getUser, removeUserSession } from './Common'


 const Nave = (props) => {
  const navigate = useNavigate()
  const handlelogin=()=>{
    removeUserSession()
    navigate('/login')
  }
  const usr=getUser();
    return (
        <div >
         
        <Navbar className="navbar-main"  variant="b">
                  <Navbar.Brand >
            <img className="logo"  src={user}  width="40px" height="40px"  />
            
                   </Navbar.Brand>
                    
          <Navbar.Toggle className="coloring" />
          
          <Navbar.Collapse className="navdiv">
            <Nav className="navdiv">
              <NavDropdown title="الاعدادات">
                <Link to="/login" onClick={handlelogin}>تسجيل الخروج</Link>
              </NavDropdown>
              <Nav.Link className="nitems" href="#blog">الدعم الفني</Nav.Link>
              <Nav.Link className="navdiv" href="#about-us">من نحن</Nav.Link>
            </Nav>
            <Navbar.Brand >
            <img src={navlogo} class="imglogo rounded-circle" ></img>
           
          </Navbar.Brand>
            
          </Navbar.Collapse>
        
        </Navbar>
       
      </div>
    
    )
}

 export default Nave